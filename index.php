<?php
// 1. Общие настройки
session_start();

// 2. Подключение файлов к системе
require_once('./config/config.php');
require_once(ROOT . '/components/Autoload.php');

// 3. Назначаем обработчик ошибок
new ErrorHandler();

// 4. Вызов Router
$router = new Router();
$router -> run();
