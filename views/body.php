<div class="main">

    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Lorem ipsum</h1>
            <p class="lead text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias aut consectetur consequuntur debitis dolor ducimus eaque facere laborum, molestias nam odio odit perferendis possimus rem tempore tenetur vitae voluptas?</p>
            <p>
                <?php
                    if(!isset($_COOKIE['user']) && !$_COOKIE['user'] == 'true'){
                        echo '<p><a class="btn btn-primary my-2" href="' . $this->link  . '">Аутентификация через ВКонтакте</a></p>';
                    }
                    ?>
            </p>
        </div>
    </section>

    <div class="container-flex photo">
        <?php
        if(!isset($_COOKIE['user']) && !$_COOKIE['user'] == 'true') {
            echo '<div class="card-deck">';
            foreach ($this->users as $volume => $item) {
                foreach ($item as $key => $value) {
                    echo
                        '
                            <div class="card">
                                 <img src="' . $value['photo_200_orig'] . '" class="card-img-top" alt="...">
                                    <div class="card-body">
                                            <h5 class="card-title">' . $value['first_name'] . '' . ' ' . $value['last_name'] . ' </h5>
                                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae eaque excepturi ipsam maxime, quidem quis tempora voluptas. Alias earum modi quisquam reiciendis veniam. Atque blanditiis illum obcaecati, sint velit voluptatibus.</p>
                                    </div>
                             <div class="card-footer">
                            <small class="text-muted"><a class="btn btn-primary my-2" href="https://vk.com/id' . $value['id'] . '">Зайти в гости</a></small>
                            </div>
                        </div>
                        ';

                }
            }
            echo '</div>';
        }else{
            echo '<div class="card-deck">';
            foreach (unserialize($_COOKIE['friends']) as $item =>$value){
                echo
                    '<div class="card">
                                 <img src="' . $value['photo'] . '" class="card-img-top" alt="...">
                                    <div class="card-body">
                                            <h5 class="card-title">' . $value['first_name'] . '' . ' ' . $value['lastname'] . ' </h5>
                                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae eaque excepturi ipsam maxime, quidem quis tempora voluptas. Alias earum modi quisquam reiciendis veniam. Atque blanditiis illum obcaecati, sint velit voluptatibus.</p>
                                    </div>
                             <div class="card-footer">
                            <small class="text-muted"><a class="btn btn-primary my-2" href="https://vk.com/id' . $item . '">Зайти в гости</a></small>
                            </div>
                        </div>';
            }
            echo '</div>';
        }?>
        </div>
    </div>
</div>
