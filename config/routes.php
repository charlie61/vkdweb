<?php

/**
 * Маршруты приложения
 */

return array(
    "^([0-9A-Za-z-_\?=]+)$" => "main/index",
    "index.php" => "main/index", // actionIndex в MainController
    "index" => "main/index", // actionIndex в MainController
    "" => 'main/index', // actionIndex в MainController
)
?>