<?php
/**
 * Определение констант
 */

define('DEBUG', 0);
define('MAIN', 'vkdweb/');
define('REQUEST_SCHEME', $_SERVER['REQUEST_SCHEME']);
define('HOST', $_SERVER['HTTP_HOST']);
define('DOMAIN', REQUEST_SCHEME . '://' . HOST . '/');
define('ROOT', dirname(__DIR__));
define('LOG', ROOT  . '/log');
define('VIEWS', ROOT . '/views');
$app_path = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['PHP_SELF']}";

$app_path = preg_replace("#[^/]+$#", '', $app_path);

$app_path = str_replace('/public/', '', $app_path);
define("PATH", $app_path);
