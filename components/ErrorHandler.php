<?php

/**
 * Класс обработки ошибок
 */

class ErrorHandler{

    /**
     * Получение настроек для обработчика ошибок
     */

    public function __construct(){
        if(DEBUG){
            error_reporting(-1);
        }else{
            error_reporting(0);
        }
        set_exception_handler([$this, 'exceptionHandler']);
    }


    /**
     * Передача параметров служебным функциям
     *
     */

    public function exceptionHandler($e){
        $this->writeErrLog($e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode());
        $this->displayError( 'Исключение',$e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode());

    }


    /**
     * Функция вывода ошибки на экран
     *
     */

    protected function displayError($errorno, $errstr, $errfile, $errline, $responce = 404){

        http_response_code($responce);
        if($responce == 404 && !DEBUG){
            require VIEWS . '/errors/404.php';
            die;
        }
        if(DEBUG){
            require VIEWS .'/errors/dev.php';
        }else{
            require VIEWS . '/errors/prod.php';
        }
        die;
    }


    /**
     * Функция записи ошибки в файл лога ошибок
     *
     */

    protected function writeErrLog ($message = '', $file = '', $line = '', $code = '') {
        error_log("[" . date('Y-m-d H:i:s') . "] Текст ошибки: {$message} | Файл: {$file} | Строка: {$line} | Код {$code} \n=================\n", 3, ROOT . '/tmp/errors.log');
    }

}