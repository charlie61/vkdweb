<?php

/**
 * Автозагрузчик классов приложения
 */

spl_autoload_register(function ($className) {
    $arr_directories = array(
        '/components/',
        '/models/',
        '/controllers/'
    );

    foreach ($arr_directories as $directory) {
        $path = ROOT . $directory . $className . '.php';
        if (is_file($path)) {
            include_once($path);
            break;
        }
    }

});
?>