<?php

/**
 * Класс роутинга
 */

class Router {

    private $routes;

    public function __construct() {
        $routesPath = ROOT . '/config/routes.php';
        $this->routes = include($routesPath);

    }

    public function run() {
        // Получаем строку запроса
        $uri = trim($_SERVER['REQUEST_URI'], '/');
        // Проверяем наличие такого маршрута
        foreach($this->routes AS $uriPattern => $path) {
            if (preg_match("~$uriPattern~", $uri)) {
                // Если есть совпадение то анализируем
                $internalPath = preg_replace("~$uriPattern~", $path, $uri);
                $segments = explode('/', $internalPath);
                $controllerName = array_shift($segments) . 'Controller';
                $controllerName = ucfirst($controllerName);
                $actionName = 'action' . ucfirst(array_shift($segments));
                $parameters = $segments;
                // Подключаем класс
                $controllerFile = ROOT . "/controllers/$controllerName.php";
                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }
                // Вызываем action
                $controllerObject = new $controllerName;
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
                if ($result != null) {
                    break;
                }
            }
        }

    }

}
