<?php

/**
 * Класс контроллера главной страницы
 */

class Main extends Model
{
    public $link;
    public $users;
    public $user;
    public $friends;

    public $userInfo = [];
    public $client_id = '7031793'; // ID приложения
    public $client_secret = 'QR4ErOMhp0oE5MGnVUVq'; // Защищённый ключ
    public $redirect_uri = 'https://vkdweb.000webhostapp.com/'; // Адрес сайта
    public $token = '';
    public $url = 'https://oauth.vk.com/authorize';


    /**
     * Функция для получения данных о друзьях пользователя
     */
    public function setAll()
    {
        if(!isset($_COOKIE['user']) && !$_COOKIE['user'] == 'true') {
            $params = [
                'scope' => 'wall',
                'client_id' => $this->client_id,
                'redirect_uri' => $this->redirect_uri,
                'response_type' => 'code'
            ];
            $this->link = $this->url . '?' . urldecode(http_build_query($params));

            if (isset($_GET['code'])) {
                $params = array(
                    'client_id' => $this->client_id,
                    'client_secret' => $this->client_secret,
                    'code' => $_GET['code'],
                    'redirect_uri' => $this->redirect_uri
                );
                $tokenTmp = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);

                if (isset($tokenTmp['access_token'])) {
                    $params = array(
                        'uids' => $tokenTmp['user_id'],
                        'fields' => 'photo_200_orig',
                        'access_token' => $tokenTmp['access_token'],
                        'count' => 5,
                        'v' => '5.8'
                    );
                    $userInfo = json_decode(file_get_contents('https://api.vk.com/method/friends.get' . '?' . urldecode(http_build_query($params))), true);
                    $this->users = $userInfo['response'];
                    setcookie('user', 'true', time()+60);
                    $friends = [];
                    foreach ($userInfo['response'] as $volume => $item) {
                        foreach ($item as $key => $value) {
                            $friends[$value['id']] = ['photo' => $value['photo_200_orig'], 'lastname' => $value['last_name'], 'first_name' => $value['first_name'] ];
                        }
                    }
//                    print_r($friends);
                    $this->friends = $friends;
                    setcookie('friends', serialize($friends), time()+60);
//                    setcookie('my_friends', $friends, time()+60);
                }
            }
        }

        include_once (VIEWS . '/header.php');
        include_once (VIEWS . '/body.php');
        include_once (VIEWS . '/footer.php');
    }
}